//
//  MainViewController.swift
//  SensitiveInformationPOC
//
//  Created by Joey Hsieh on 2022/11/29.
//

import UIKit

class MainViewController: UIViewController {
    override func viewDidLoad() {
        super.viewDidLoad()

        setupNotification()
    }

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        if isRecording() {
            createNotifcation(title:"螢幕錄影中")
        }
    }

    func setupNotification() {
        NotificationCenter.default.addObserver(self, selector: #selector(didTakeScreenshot(notification:)), name: UIApplication.userDidTakeScreenshotNotification, object: nil)

        NotificationCenter.default.addObserver(self, selector: #selector(didRecording(notification:)), name: UIScreen.capturedDidChangeNotification, object: nil)
    }

    @objc func didTakeScreenshot(notification: Notification) {
        print("擷取螢幕")
        createNotifcation(title:"取得螢幕截圖")
    }

    @objc func didRecording(notification: Notification) {
        if UIScreen.main.isCaptured {
            print("螢幕錄影中")
            createNotifcation(title:"螢幕錄影中")
        }
    }

    func isRecording() -> Bool {
        for screen in UIScreen.screens {
            if screen.isCaptured {
                print("螢幕錄影中")
                return true
            }
        }
        return false
    }

    func createNotifcation(title: String) {
        let content = UNMutableNotificationContent()
        content.title = title
        let request = UNNotificationRequest(identifier: "notification", content: content, trigger: nil)
        UNUserNotificationCenter.current().add(request, withCompletionHandler: { _ in
            print("create screenshot notification")
        })
    }
}
